﻿using EAPExpertV4DB.Attributes;
using EAPExpertV4DB.Models;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;

namespace EAPExpertV4DB
{
    public class Program
    {
        static void Main(string[] args)
        {
            using (var db = new V4Context())
            {
                StateProvince prov = new StateProvince();
                prov.CreatedBy = "Dave";
                prov.CreatedOn = System.DateTime.Now;
                db.StateProvince.Add(prov);
                db.SaveChanges();
            }
        }

    }

    public class V4Context : DbContext
    {
        public DbSet<Address> Address { get; set; }
        public DbSet<ContactPerson> ContactPerson { get; set; }
        public DbSet<Country> Country { get; set; }        
        public DbSet<PresentingProblem> PresentingProblem { get; set; }
        public DbSet<PresentingProblemGroup> PresentingProblemGroup { get; set; }
        public DbSet<StateProvince> StateProvince { get; set; }
        public DbSet<Title> Title { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            List<string> CoreProperties = new List<string>() { "ObjectId", "IsDeleted", "CreatedOn", "CreatedBy", "ModifiedOn", "ModifiedBy", "DeletedOn", "DeletedBy", "IsActive", "SortOrder", "Description" };
            int columnOrder = 2;
            modelBuilder.Properties<string>().Configure(s => s.HasMaxLength(100));            
            modelBuilder.Properties().Where(x => !CoreProperties.Contains(x.Name)).Configure(s => s.HasColumnOrder(columnOrder++));            
            modelBuilder.Properties<string>().Where(p => p.CustomAttributes.Any(a => typeof(MemoAttribute).IsAssignableFrom(a.AttributeType))).Configure(p => p.HasColumnType("nvarchar(max)"));
            modelBuilder.Conventions.Add(new ForeignKeyNamingConvention());
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }
    }

    public class ForeignKeyNamingConvention : IStoreModelConvention<AssociationType>
    {

        public void Apply(AssociationType association, DbModel model)
        {
            // Identify a ForeignKey properties (including IAs)
            if (association.IsForeignKey)
            {
                // rename FK columns
                var constraint = association.Constraint;
                if (DoPropertiesHaveDefaultNames(constraint.FromProperties, constraint.ToRole.Name, constraint.ToProperties))
                {
                    NormalizeForeignKeyProperties(constraint.FromProperties);
                }
                if (DoPropertiesHaveDefaultNames(constraint.ToProperties, constraint.FromRole.Name, constraint.FromProperties))
                {
                    NormalizeForeignKeyProperties(constraint.ToProperties);
                }
            }
        }

        private bool DoPropertiesHaveDefaultNames(ReadOnlyMetadataCollection<EdmProperty> properties, string roleName, ReadOnlyMetadataCollection<EdmProperty> otherEndProperties)
        {
            if (properties.Count != otherEndProperties.Count)
            {
                return false;
            }

            for (int i = 0; i < properties.Count; ++i)
            {
                if (!properties[i].Name.EndsWith("_" + otherEndProperties[i].Name))
                {
                    return false;
                }
            }
            return true;
        }

        private void NormalizeForeignKeyProperties(ReadOnlyMetadataCollection<EdmProperty> properties)
        {
            for (int i = 0; i < properties.Count; ++i)
            {
                string defaultPropertyName = properties[i].Name;
                int ichUnderscore = defaultPropertyName.IndexOf('_');
                if (ichUnderscore <= 0)
                {
                    continue;
                }
                string navigationPropertyName = defaultPropertyName.Substring(0, ichUnderscore);
                string targetKey = defaultPropertyName.Substring(ichUnderscore + 1);

                string newPropertyName;
                if (targetKey.StartsWith(navigationPropertyName))
                {
                    newPropertyName = targetKey;
                }
                else
                {
                    newPropertyName = navigationPropertyName + targetKey;
                }
                properties[i].Name = newPropertyName;
            }
        }

    }
}
