﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("Address")]
    public class Address : BaseObject
    {   
        public string Street { get; set; }

        public string Suite { get; set; }

        public string City { get; set; }
        public StateProvince StateProvince { get; set; }
        public int StateProvinceId { get; set; }
        public Country Country { get; set; }
        public int CountryId { get; set; }

        [MaxLength(10)]
        public string ZipPostalCode { get; set; }

        public float Longitude { get; set; }

        public float Latitude { get; set; }

        public int AddressType { get; set; }
    }
}
