﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("PhoneNumber")]
    public class PhoneNumber : BaseObject
    {
        public string Number { get; set; }

        public PhoneRestriction PhoneRestriction { get; set; }

        public int PhoneRestrictionId { get; set; }

        public int PhoneType { get; set; }
    }
}
