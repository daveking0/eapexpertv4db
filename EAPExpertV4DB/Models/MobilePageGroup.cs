﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("MobilePageGroup")]
    public class MobilePageGroup
    {
        public ICollection<MobilePage> MobilePages { get; set; }
    }
}
