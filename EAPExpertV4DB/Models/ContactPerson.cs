﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("ContactPerson")]
    public class ContactPerson : BaseObject
    {
        public Title Title { get; set; }
        public int TitleId { get; set; }
        public string FirstName { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }
        
        public string PartnerName { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public string SocialSecurityNumber { get; set; }

        public string EmployeeNumber { get; set; }

        public string AgeRange { get; set; }

        public ICollection<Address> Addresses { get; set; }

        public ICollection<EmailAddress> EmailAddresses { get; set; }

        public ICollection<PhoneNumber> PhoneNumbers { get; set; }

        public TimeZone TimeZone { get; set; }

        public int TimeZoneId { get; set; }
    }
}
