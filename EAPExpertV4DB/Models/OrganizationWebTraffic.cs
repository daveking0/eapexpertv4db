﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("OrganizationWebTraffic")]
    public class OrganizationWebTraffic : BaseObject
    {
        public DateTime EffectiveDate { get; set; }
        public int TrafficCount { get; set; }
        public int Serviced { get; set; }

        public WebPage WebPage { get; set; }
        public int WebPageId { get; set; }

        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }

    }
}
