﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{   
    public abstract class CustomField : BaseObject
    {   
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public int SortOrder { get; set; }

        [MaxLength(200)]
        public string Description { get; set; }
    }
}
