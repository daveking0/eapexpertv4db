﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("OrganizationMobileTraffic")]
    public class OrganizationMobileTraffic : BaseObject
    {
        public DateTime EffectiveDate { get; set; }
        public int TrafficCount { get; set; }
        public int Serviced { get; set; }

        public MobilePage MobilePage { get; set; }
        public int MobilePageId { get; set; }

        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }
    }
}
