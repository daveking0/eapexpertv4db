﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("ClientFile")]
    public class ClientFile : BaseObject
    {
        public ClientType ClientType { get; set; }
        public int ClientTypeId { get; set; }
        public Client Client { get; set; }
        public int ClientId { get; set; }
        public bool Primary { get; set; }
        public bool ServiceRequester { get; set; }
        public bool ServiceUser { get; set; }
        public Relationship Relationship { get; set; }
        public int RelationshipId { get; set; }
    }
}
