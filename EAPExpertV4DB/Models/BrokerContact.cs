﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("BrokerContact")]
    public class BrokerContact : ContactPerson
    {
        public BrokerType BrokerType { get; set; }
        public int BrokerTypeId { get; set; }

        public bool PrimaryContact { get; set; }
        public ICollection<Organization> Organizations { get; set; }

        public BrokerFirm BrokerFirm { get; set; }
        public int BrokerFirmId { get; set; }
    }
}
