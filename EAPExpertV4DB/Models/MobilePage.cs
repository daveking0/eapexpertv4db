﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("MobilePage")]
    public class MobilePage : CustomField
    {
        public MobilePageGroup MobilePageGroup { get; set; }
    }
}
