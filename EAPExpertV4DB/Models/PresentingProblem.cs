﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("PresentingProblem")]
    public class PresentingProblem : CustomField
    {        
        public PresentingProblemGroup PresentingProblemGroup { get; set; }
    }
}
