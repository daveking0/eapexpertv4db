﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("OrganizationContact")]
    public class OrganizationContact : ContactPerson
    {
        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }
        public bool PrimaryContact { get; set; }

        public ContactLevel ContactLevel { get; set; }
        public int ContactLevelId { get; set; }

        public Occupation Occupation { get; set; }
        public int OccupationId { get; set; }

        public Modality PrimaryModality { get; set; }
        public int PrimaryModalityId { get; set; }

        public Modality SecondaryModality { get; set; }
        public int SecondaryModalityId { get; set; }
    }
}
