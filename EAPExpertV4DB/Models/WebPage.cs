﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("WebPage")]
    public class WebPage : CustomField
    {
        public WebPageGroup WebPageGroup { get; set; }
    }
}
