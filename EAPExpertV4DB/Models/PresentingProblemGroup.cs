﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("PresentingProblemGroup")]
    public class PresentingProblemGroup : CustomField
    {   
        public ICollection<PresentingProblem> PresentingProblems { get; set; }
    }
}
