﻿using EAPExpertV4DB.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("Organization")]
    public class Organization : BaseObject
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Number { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string Website { get; set; }
        public string AccessCode { get; set; }
        public string UserName { get; set; }
        public bool Active { get; set; }
        public OrganizationEnvironment Environment { get; set; }
        public int EnvironmentId { get; set; }
        public Industry Industry { get; set; }
        public int IndustryId { get; set; }                
        public AccountType AccountType { get; set; }
        public int AccountTypeId { get; set; }        
        public DateTime ClientInceptionDate { get; set; }
        public DateTime ClientTerminationDate { get; set; }
        public DateTime ProposalSentDate { get; set; }
        
        public Clinical ProgramManager { get; set; }
        public int ProgramManagerId { get; set; }
                
        public Clinical BackupProgramManager { get; set; }
        public int BackupProgramManagerId { get; set; }
                
        public Clinical AccountRepresentative { get; set; }
        public int AccountRepresentativeId { get; set; }        
        public bool IsSubContract { get; set; }     
        public OfficeLocation PrimaryOfficeOfService { get; set; }
        public int PrimaryOfficeOfServiceId { get; set; }
                
        public CustomImage CompanyLogo { get; set; }        
        public int CompanyLogoId { get; set; }
                
        public CustomImage AdditionalCompanyLogo { get; set; }
        public int AdditionalCompanyLogoId { get; set; }

        [MemoAttribute()]
        public string IntakeInfo { get; set; }

        [MemoAttribute()]
        public string HighRiskInfo { get; set; }

        public ICollection<OrganizationContact> OrganizationContacts { get; set; }
        public ICollection<BrokerContact> BrokerContacts { get; set; }
        public ICollection<EmployeePopulation> Populations { get; set; }
        public ICollection<OrganizationWebTraffic> WebTraffic { get; set; }
        public ICollection<OrganizationMobileTraffic> MobileTraffic { get; set; }
    }
}
