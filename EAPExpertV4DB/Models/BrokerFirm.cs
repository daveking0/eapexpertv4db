﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("BrokerFirm")]
    public class BrokerFirm : BaseObject
    {
        public string Name { get; set; }
        public ICollection<Address> Addresses { get; set; }
        public ICollection<EmailAddress> EmailAddresses { get; set; }
        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
        public ICollection<BrokerContact> BrokerContacts { get; set; }
    }
}
