﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("EmployeePopulation")]
    public class EmployeePopulation : BaseObject
    {
        public DateTime PopulationDate { get; set; }
        public int PopulationCount { get; set; }
        public Organization Organization { get; set; }
        public int OrganizationId { get; set; }
    }
}
