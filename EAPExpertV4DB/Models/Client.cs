﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("Client")]
    public class Client : ContactPerson
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Number { get; set; }

        public ICollection<ClientFile> ClientFiles { get; set; }
    }
}
