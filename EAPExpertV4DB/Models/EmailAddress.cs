﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    [Table("EmailAddress")]
    public class EmailAddress : BaseObject
    {
        public ContactPerson ContactPerson { get; set; }
        public int ContactPersonId { get; set; }
        public string Address { get; set; }
        public int Type { get; set; }
    }
}
