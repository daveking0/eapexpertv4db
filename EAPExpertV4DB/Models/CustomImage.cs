﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("CustomImage")]
    public class CustomImage
    {
        [Key]
        [Column(Order = 0)]
        public int Id { get; set; }
        public byte[] Image { get; set; }
    }
}
