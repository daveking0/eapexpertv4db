﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("WebPageGroup")]
    public class WebPageGroup : CustomField
    {
        public ICollection<WebPage> WebPages { get; set; }
    }
}
