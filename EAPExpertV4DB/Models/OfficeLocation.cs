﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EAPExpertV4DB.Models
{
    public class OfficeLocation : BaseObject
    {
        public string Name { get; set; }
        public string ContactPerson { get; set; }
        public bool Active { get; set; }
        public bool IsInternal { get; set; }
        public ICollection<Address> Addresses { get; set; }
        public ICollection<EmailAddress> EmailAddresses { get; set; }
        public ICollection<PhoneNumber> PhoneNumbers { get; set; }
    }
}
