﻿using System.ComponentModel.DataAnnotations.Schema;

namespace EAPExpertV4DB.Models
{
    [Table("OrganizationAlias")]
    public class OrganizationAlias : BaseObject
    {
        public string Name { get; set; }
    }
}
